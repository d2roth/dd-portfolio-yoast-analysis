(function($) {
 
    var portfolioYoastPlugin = function()
    {
        YoastSEO.app.registerPlugin('portfolioYoastPlugin', {status: 'loading'});
 
        this.getData();
    };
 
    portfolioYoastPlugin.prototype.getData = function()
    {
 
        var _self = this;
 
        YoastSEO.app.pluginReady('portfolioYoastPlugin');
 
        YoastSEO.app.registerModification('content', $.proxy(_self.getCustomContent, _self), 'portfolioYoastPlugin', 5);
 
    };
 
    portfolioYoastPlugin.prototype.getCustomContent = function (content)
    {
      var mceid = $('#wp-_nectar_portfolio_extra_content-editor-container textarea').prop('id');
      return tinymce.editors[mceid].getContent() + content;
    };
 
    $(window).on('YoastSEO:ready', function ()
    {
      new portfolioYoastPlugin();
    });

})(jQuery);