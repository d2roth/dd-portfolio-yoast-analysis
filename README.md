# README #

This plugin is based off of Paul Robinson's tutorial [here](https://return-true.com/adding-content-to-yoast-seo-analysis-using-yoastseojs/).

### What is this repository for? ###

* This plugin enables the content of Nectre's Portfolio post type to be used with Yoast SEO's analytics.
* Version 0.1