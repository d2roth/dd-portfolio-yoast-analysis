<?php

/*
  Plugin Name:  DD Portfolio Yoast Analysis
  Plugin URI:   https://bitbucket.org/d2roth/dd-portfolio-yoast-analysis
  Description:  Adds portfolio content to Yoast SEO for analysis
  Version:      0.1
  Author:       Daniel Roth
  License:      GPL2
  License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function dd_portfolio_analysis_enqueue_scripts(){
	if(get_post_type() == "portfolio"){
  	wp_enqueue_script('dd_portfolio_yoast_plugin', plugins_url('js/portfolio-yoast-plugin.js', __FILE__), array('yoast-seo-admin-script'));
  }
}
add_action('admin_enqueue_scripts', 'dd_portfolio_analysis_enqueue_scripts');

